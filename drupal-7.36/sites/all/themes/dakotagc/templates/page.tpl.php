<div id="fb-root"></div>
	<script>(function(d, s, id) 
	{
			  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
		  fjs.parentNode.insertBefore(js, fjs);
	}
		(document, 'script', 'facebook-jssdk'));
     </script>

<div id="intro"><div id="intro2"><div id="intro3"><div id="intro4">
        <img src="/DGC/drupal-7.36/<?php print path_to_theme(); ?>/images/banner.png" /></div></div></div></div><!-- intro -->
<div id="menu">

            <ul class="ulInner">
				<li><a href="https://www.facebook.com/dakotagamecon?fref=ts" target="new">
                <img src="/DGC/drupal-7.36/<?php print path_to_theme(); ?>/images/facebook1.png" width="25" height="25" alt="Facebook" /></a></li>
                <li><div class="fb-like" data-href="https://www.facebook.com/dakotagamecon?fref=ts" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div></li>
                <?php 
                    print theme('links',array('links'=>$main_menu));
                ?>
            </ul>
        </div>
<div id="main"><div id="main2">
        <div id="content">
            <?php if ($messages): ?>
                <div id="messages">
                  <?php print $messages; ?>
                </div>
            <?php endif; ?>

            <?php if ($tabs): ?>
                <div class="tabs">
                    <?php print render($tabs); ?>
                </div>
            <?php endif; ?>

            <?php
                print render($page['content']);
            ?>
        </div><!-- content -->
        <div id="sidebar">
            <?php
                print render($page['col']);
            ?>
        </div><!-- sidebar -->
        <div class="clearing">&nbsp;</div>
</div></div><!-- main --><!-- main2 -->
<div id="bar"><div id="bar2">
        <div class="col">
            
            <a href="http://visitbrookingssd.com/"><img src="/DGC/drupal-7.36/<?php print path_to_theme(); ?>/images/cvb.jpg"width="346" height="144" alt="" /></a>
            <p>Dakota Game Convention is sponsored by The Brookings Convention and Visitors Bureau. The Brookings Convention &amp; Visitors Bureau has been in existence since 1982 operating as a committee of the Brookings Area Chamber of Commerce.</p>
            
        </div>  
        <div class="col">
            
            <a href="http://www.crunchyroll.com/"><img src="/DGC/drupal-7.36/<?php print path_to_theme(); ?>/images/crunchyroll.png"width="346" height="144"  alt="" /></a>
            <p>Anime viewing sponsored by Crunchyroll.</p>
            
        </div>
        <div class="col lastcol">
            
            <a href="http://www.swiftelcenter.com/"><img src="/DGC/drupal-7.36/<?php print path_to_theme(); ?>/images/swiftelcenter.png"width="346" height="144"  alt="" /></a>
            <p>The Swiftel Center has established itself as one of South Dakota's premier event centers for meetings and entertainment. With our 30,000 square foot arena, state of the art Daktronics Banquet Rooms, Concourse, County Rooms, Conference Rooms and In-House Catering service; we can plan any event that you can imagine.</p>
            
        </div>
        <div class="clearingBottom"></div>
</div></div><!-- bar --><!-- bar2 -->

<div id="footer">

    <div id="footer2">
    
        <p>Copyright &copy; 2015 Dakota Game Con</p>
    </div>
</div>
